﻿using BuyOrSellSocks.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.ViewModel
{
    public class UserListViewModel
    {
        public UserListViewModel() 
        {
            this.Users = User.GetUsers();
        }
        private List<User> users { get; set; }
        public List<User> Users
        {
            get
            {
                return this.users;
            }
            set
            {
                this.users = value;
            }
        }
    }
}
