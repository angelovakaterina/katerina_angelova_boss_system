﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.ViewModel
{
    public class LoginViewModel
    {
        private string username;
        private string password;

        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
               
        }
        public string Password 
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        public void Login() 
        {
            Model.User.Login(username, password);
        }
    }
}
