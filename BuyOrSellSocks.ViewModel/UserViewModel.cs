﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.ViewModel
{
    public class UserViewModel
    {
        private string username { get; set; }
        private string password { get; set; }
        private int roleId { get; set; }

        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        public int RoleId
        {
            get
            {
                return this.roleId;
            }
            set
            {
                this.roleId = value;
            }
        }

        public void SaveUser()
        {
            Model.User.CreateUser(new Model.User() { Username = this.username, Passsword = this.password, RoleId = this.roleId});
        }

        public void DeleteUser()
        {
            Model.User.Login(username, password);
        }
    }
}
