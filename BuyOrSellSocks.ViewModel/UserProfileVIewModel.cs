﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.ViewModel
{
    public class UserProfileVIewModel
    {

        private string username { get; set; }
        private decimal moneyBalance { get; set; }
        private int sockBalance { get; set; }

        public string Username {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }
        public decimal MoneyBalance {
            get
            {
                return this.moneyBalance;
            }
            set
            {
                this.moneyBalance = value;
            }
        }
        public int SockBalance {
            get
            {
                return this.sockBalance;
            }
            set
            {
                this.sockBalance = value;
            }
        }
    }
}
