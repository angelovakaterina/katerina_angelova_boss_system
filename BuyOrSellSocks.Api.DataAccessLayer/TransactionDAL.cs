﻿using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.DataAccessLayer
{
    public class TransactionDAL
    {
        private BuyOrSellSocksEntities buyOrSellSocksEntities = null;

        public TransactionDAL()
        {
            this.buyOrSellSocksEntities = new BuyOrSellSocksEntities();
        }

        public bool MoneyTransaction(int userId, decimal money)
        {
            bool success = false;
            try
            {
               var res = buyOrSellSocksEntities.UserMoney.Add(new UserMoney() { UserId = userId, Money = money });
                success = (res == null) ? false : true;
            }
            catch (Exception exception)
            {
                //todo add login 
                return success;
            }
            return success;
        }

        public bool SocksTransaction(int userId, int quantity, decimal price)
        {
            bool success = false;
            try
            {
                var res = buyOrSellSocksEntities.UserSocks.Add(new UserSocks() { UserId = userId, Price = price, Quantity = quantity });
                success = (res == null) ? false : true;
            }
            catch (Exception exception)
            {
                //todo add login 
                return success;
            }
            return success;
        }

        public int GetSocksAmount(int userId)
        {
            int socks = 0;
            try
            {
                socks = (from u in buyOrSellSocksEntities.UserSocks
                         where u.UserId == userId
                         select u).Sum(x => x.Quantity);
            }
            catch (Exception exception)
            {
                //todo add login 
                return socks;
            }
            return socks;
        }

        public decimal GetMoneyAmount(int userId)
        {
            decimal money = 0M;
            try
            {
                money = (from u in buyOrSellSocksEntities.UserMoney
                         where u.UserId == userId
                         select u).Sum(x => x.Money);
            }
            catch (Exception exception)
            {
                //todo add login 
                return money;
            }
            return money;
        }

    }
}
