﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.DataAccessLayer
{
    public class LoginDAL
    {
        private BuyOrSellSocksEntities buyOrSellSocksEntities = null;

        public LoginDAL()
        {
            this.buyOrSellSocksEntities = new BuyOrSellSocksEntities();
        }
        public bool Login(string username, string password)
        {
            bool success = false;
            try
            {
                User user = (from u in buyOrSellSocksEntities.User
                             where u.Username.ToUpper() == username.ToUpper()
                             where u.Password == password
                             select u).SingleOrDefault();

                success = (user == null) ? false : true;
            }
            catch (Exception exception)
            {
                //todo add loging module
                return success;
            }
            return success;
        }
    }
}
