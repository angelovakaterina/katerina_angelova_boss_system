﻿using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.DataAccessLayer
{
    public class UserDAL
    {
        private BuyOrSellSocksEntities buyOrSellSocksEntities = null;

        public UserDAL()
        {
            this.buyOrSellSocksEntities = new BuyOrSellSocksEntities();
        }

        public List<UserDTO> GetUsers()
        {
            List<UserDTO> usersDto = new List<UserDTO>();
            try
            {
                usersDto = (from u in buyOrSellSocksEntities.User
                            select new UserDTO()
                            {
                                id = u.Id,
                                username = u.Username,
                                password = u.Password,
                                roleId = u.RoleId
                            }).ToList<UserDTO>();
            }
            catch (Exception exception)
            {
                //todo add loging module
            }
            return usersDto;
        }

        public UserDTO GetUser(int id)
        {
            UserDTO userDto = new UserDTO();
            try
            {
                userDto = (from u in buyOrSellSocksEntities.User
                           where u.Id == id
                           select new UserDTO()
                           {
                               id = u.Id,
                               username = u.Username,
                               password = u.Password,
                               roleId = u.RoleId
                           }).SingleOrDefault();
            }
            catch (Exception exception)
            {
                //todo add loging module
            }
            return userDto;
        }

        public UserDTO CreateUser(UserDTO userDto)
        {
            User savedUser = null;
            try
            {
                User user = new User();
                user.Username = userDto.username;
                user.Password = userDto.password;
                user.RoleId = userDto.roleId;

                buyOrSellSocksEntities.User.AddOrUpdate(user);

                savedUser = (from u in buyOrSellSocksEntities.User
                             where u.Id == userDto.id
                             select u).SingleOrDefault();

                userDto.id = savedUser.Id;
            }
            catch (Exception exception)
            {
                //todo add login 
                return new UserDTO();
            }
            return userDto;
        }

        public UserDTO UpdateUser(UserDTO userDto)
        {
            if (userDto == null && userDto.id == 0 && userDto.username == null && userDto.password == null && userDto.roleId == 0)
            {
                return new UserDTO();
            }

            try
            {
                User savedUser = (from u in buyOrSellSocksEntities.User
                                  where u.Id == userDto.id
                                  select u).SingleOrDefault();
                if (savedUser != null)
                {
                    savedUser.Username = userDto.username;
                    savedUser.Password = userDto.password;
                    savedUser.RoleId = userDto.roleId;

                    buyOrSellSocksEntities.User.AddOrUpdate(savedUser);
                }
            }
            catch (Exception exception)
            {
                //todo add login 
                return new UserDTO();
            }
            return userDto;
        }

        public bool DeleteUser(int id)
        {
            bool success = false;
            try
            {
                User user = (from u in buyOrSellSocksEntities.User
                             where u.Id == id
                             select u).SingleOrDefault();
                User deletedUser = buyOrSellSocksEntities.User.Remove(user);
                success = (deletedUser == null) ? false : true;
            }
            catch (Exception exception)
            {
                //todo add login 
                return success;
            }
            return success;
        }       
    }
}
