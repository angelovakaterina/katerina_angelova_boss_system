﻿using BuyOrSellSocks.Api.DataAccessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.BusinessLayer
{
    public class TransactionBL
    {
        private TransactionDAL transactionDAL = null;

        public TransactionBL()
        {
            this.transactionDAL = new TransactionDAL();
        }

        public bool MoneyTransaction(int userId, decimal money)
        {
            return this.transactionDAL.MoneyTransaction(userId, money);
        }

        public bool SocksTransaction(int userId, int quantity, decimal price)
        {
            return this.transactionDAL.SocksTransaction(userId, quantity, price);

        }
    }
}
