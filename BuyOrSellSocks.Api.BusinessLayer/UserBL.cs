﻿using BuyOrSellSocks.Api.DataAccessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.BusinessLayer
{
    public class UserBL
    {
        private UserDAL userDal = null;
        private TransactionDAL transactionDAL = null;

        public UserBL()
        {
            this.userDal = new UserDAL();
            this.transactionDAL = new TransactionDAL();
        }

        public List<UserDTO> GetUsers()
        {
            return this.userDal.GetUsers();
        }

        public UserDTO GetUser(int id)
        {
            return this.userDal.GetUser(id);
        }

        public UserProfileDTO GetUserProfile(int id)
        {
            var user = this.userDal.GetUser(id);
            var userProfileDto = new UserProfileDTO();
            try {
                userProfileDto.id = user.id;
                userProfileDto.username = user.username;
                userProfileDto.moneyBalance = this.transactionDAL.GetMoneyAmount(id);
                userProfileDto.sockBalance = this.transactionDAL.GetSocksAmount(id);
            } 
            catch(Exception ex) 
            {

            }
            
            return userProfileDto;
        }

        public UserDTO CreateUser(UserDTO user)
        {
            return this.userDal.CreateUser(user);
        }

        public UserDTO UpdateUser(UserDTO user)
        {
            return this.userDal.UpdateUser(user);
        }

        public bool DeleteUser(int id)
        {
            return this.userDal.DeleteUser(id);
        }
    }
}
