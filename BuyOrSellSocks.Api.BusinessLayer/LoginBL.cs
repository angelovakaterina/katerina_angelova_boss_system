﻿using BuyOrSellSocks.Api.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.BusinessLayer
{
    public class LoginBL
    {
        private LoginDAL loginDal = null;

        public LoginBL()
        {
            this.loginDal = new LoginDAL();
        }

        public bool Login(string username, string password)
        {            
            return this.loginDal.Login(username, password);
        }
    }
}
