﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Model
{
    public partial class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Passsword { get; set; }
        public int RoleId { get; set; }
    }
}
