﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Model
{
    public partial class UserProfile
    {
        public string Username { get; set; }
        public decimal MoneyBalance { get; set; }
        public int SocksBalance { get; set; }
    }
}
