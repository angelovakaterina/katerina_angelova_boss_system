﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Model.BusinessLogic
{
    public class Sock
    {
        private int hour = 0;
        private decimal currentPrice = 0;

        public decimal Price
        {
            get
            {
                if (currentPrice == 0) 
                {
                    decimal randomDecimal = 0;
                    Decimal.TryParse(new Random().NextDouble().ToString(), out randomDecimal);
                    currentPrice = randomDecimal;
                    hour = DateTime.Now.Hour;
                }
                if (hour != DateTime.Now.Hour)
                {
                    decimal randomDecimal = 0;
                    Decimal.TryParse(new Random().NextDouble().ToString(), out randomDecimal);
                    currentPrice = currentPrice * randomDecimal;
                    hour = DateTime.Now.Hour;
                }
                return currentPrice;
            }
        }
    }
}
