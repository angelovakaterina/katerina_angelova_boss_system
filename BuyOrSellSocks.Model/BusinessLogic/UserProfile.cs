﻿using BuyOrSellSocks.Api.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Model
{
    public partial class UserProfile
    {
        public UserProfileDTO GetUser(int id)
        {
            UserProfileDTO user = null;

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var responseTask = client.GetAsync($"/userprofile/{id}");
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        user = (UserProfileDTO)JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        user = new UserProfileDTO();
                    }
                }
                catch (Exception ex)
                {

                }
                return user;
            }
        }
    }
}
