﻿using BuyOrSellSocks.Api.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Model
{
    public partial class User
    {
        public static void Login(string username, string password)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var responseTask = client.GetAsync($"/login/{username}/{password}");
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        var res = JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        //false
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public static List<Model.User> GetUsers()
        {
            List<Model.User> users = null;
            
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var responseTask = client.GetAsync("/user/list");
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        users = (List<Model.User>)JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        users = Enumerable.Empty<Model.User>().ToList();
                    }
                }
                catch (Exception ex) 
                {

                }
                return users;
            }
        }

        public static Model.User GetUser(int id)
        {
            Model.User user = null;

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var responseTask = client.GetAsync($"/user/{id}");
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        user = (Model.User)JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        user = new Model.User();
                    }
                }
                catch (Exception ex)
                {

                }
                return user;
            }
        }

        public static Model.User UpdateUser(Model.User userDTO)
        {
            Model.User user = null;

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var httpContent = new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json");
                    var responseTask = client.PutAsync($"/user/{userDTO.Id}", httpContent);
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        user = (Model.User)JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        user = new Model.User();
                    }
                }
                catch (Exception ex)
                {

                }
                return user;
            }
        }
        public static Model.User CreateUser(Model.User userDTO)
        {
            Model.User user = null;

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var httpContent = new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json");
                    var responseTask = client.PostAsync($"/user/", httpContent);
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        user = (Model.User)JsonConvert.DeserializeObject(readTask.Result);
                    }
                    else
                    {
                        user = new Model.User();
                    }
                }
                catch (Exception ex)
                {

                }
                return user;
            }
        }
        public static bool DeleteUser(int id)
        {
            var user = false;

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://localhost:51763");
                    var responseTask = client.DeleteAsync($"/user/{id}");
                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                       var res = JsonConvert.DeserializeObject(readTask.Result);
                        //todo return bool
                    }
                }
                catch (Exception ex)
                {

                }
                return user;
            }
        }
    }
}
