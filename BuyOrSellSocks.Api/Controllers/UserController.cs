﻿using BuyOrSellSocks.Api.BusinessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BuyOrSellSocks.Api.Controllers
{
    [RoutePrefix("user")]
    public class UserController : ApiController
    {
        private UserBL userBL = null;

        public UserController()
        {
            this.userBL = new UserBL();
        }

        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetUsers()
        {
            return Ok(this.userBL.GetUsers());
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetUser(int id)
        {
            return Ok(this.userBL.GetUser(id));
        }


        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateUser(UserDTO userDto)
        {
            return Ok(this.userBL.CreateUser(userDto));
        }

        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult UpdateUser([FromBody] UserDTO userDto, [FromUri] int id)
        {
            return Ok(this.userBL.UpdateUser(userDto));
        }

        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult DeleteUser(int id)
        {
            return Ok(this.userBL.DeleteUser(id));
        }
    }
}
