﻿using BuyOrSellSocks.Api.BusinessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BuyOrSellSocks.Api.Controllers
{
    [RoutePrefix("transaction")]
    public class TransactionController : ApiController
    {
        private TransactionBL transactionBL = null;

        public TransactionController()
        {
            this.transactionBL = new TransactionBL();
        }

        [HttpPost]
        [Route("money/{money}")]
        public IHttpActionResult MoneyTransation(int userId, decimal money)
        {
            return Ok(this.transactionBL.MoneyTransaction(userId, money));
        }

        [HttpPost]
        [Route("socks/{quantity}/{price}")]
        public IHttpActionResult SocksTransaction(int userId, int quantity, decimal price)
        {
            return Ok(this.transactionBL.SocksTransaction(userId, quantity, price));
        }
    }
}
