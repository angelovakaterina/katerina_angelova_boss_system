﻿using BuyOrSellSocks.Api.BusinessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;

namespace BuyOrSellSocks.Api.Controllers
{
    public class LoginController : ApiController
    {
        private LoginBL loginBL = null;

        public LoginController()
        {
            this.loginBL = new LoginBL();
        }

        
        [HttpGet]
        [Route("Login/{username}/{password}")]
        public IHttpActionResult Login(string userName, string password)
        {
            return Ok(this.loginBL.Login(userName, password));
        }

    }
}
