﻿using BuyOrSellSocks.Api.BusinessLayer;
using BuyOrSellSocks.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BuyOrSellSocks.Api.Controllers
{
    [RoutePrefix("userprofile")]
    public class UserProfileController : ApiController
    {
        private UserBL userBL = null;

        public UserProfileController()
        {
            this.userBL = new UserBL();
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetUserProfile(int id)
        {
            return Ok(this.userBL.GetUserProfile(id));
        }

    }
}
