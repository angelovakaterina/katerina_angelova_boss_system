﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyOrSellSocks.Api.Dto
{
    public class UserProfileDTO
    {
        public int id { get; set; }
        public string username { get; set; }
        public decimal moneyBalance { get; set; }
        public int sockBalance { get; set; }
    }
}
